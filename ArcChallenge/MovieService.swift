//
//  MovieService.swift
//  ArcChallenge
//
//  Created by Tarek on 11/5/16.
//  Copyright © 2016 Tarek. All rights reserved.
//

import Alamofire
import AlamofireObjectMapper
import ObjectMapper

public class MovieService : Service {
    
    //
    //MARK: - Class Methods
    
    //
    //MARK: GET
    
    static func getList(success  : @escaping (Result<Movie>) -> (), failure  : ((_ error : Error) -> ())? = nil) {
        self.makeGetFrom(url: APIUrls.listMovies, success: success, failure: failure);
    }

    static func getGenre(success  : @escaping (Result<Genre>) -> (), failure  : ((_ error : Error) -> ())? = nil) {
        self.makeGetFrom(url: APIUrls.listGenre, success: success, failure: failure);
    }

    //MARK: Process Response

    private static func makeGetFrom<T:Mappable>(url: String, success : @escaping (Result<T>) -> (), failure  : ((_ error : Error) -> ())? = nil) {
        manager.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil).validate().responseObject { (response: DataResponse<Result<T>>) in
            self.processCall(success: success, failure: failure, response: response)
        }
    }
    
    private static func processCall<T: BaseMappable>(success  : @escaping (T) -> (), failure  : ((_ error : Error) -> ())? = nil, response: DataResponse<T>) {
        switch (response.result) {
        case .success(let value):
            success(value)
            break;
        case .failure(let error):
            if failure != nil {
                failure!(error)
            }
            break
        }
    }

}
