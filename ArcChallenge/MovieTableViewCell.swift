//
//  MovieTableViewCell.swift
//  ArcChallenge
//
//  Created by Tarek on 11/6/16.
//  Copyright © 2016 Tarek. All rights reserved.
//

import UIKit
import Kingfisher

class MovieTableViewCell: UITableViewCell {
    @IBOutlet var poster: UIImageView!
    @IBOutlet var title: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func bind(movie: Movie) {
        self.title.text = movie.original_title
        self.poster.kf.setImage(with: URL(string: "\(APIUrls.base_url_image)\(movie.poster_path!)"))
    }
    
}
