//
//  Movie.swift
//  ArcChallenge
//
//  Created by Tarek on 11/4/16.
//  Copyright © 2016 Tarek. All rights reserved.
//

import Foundation
import ObjectMapper

class Result<T: Mappable>: Mappable {
    var rowsAffected: Int?
    var rows: [T]?

    required init?(map: Map){ }
    
    func mapping(map: Map) {
        rowsAffected <- map["item_count"]
        rows <- map["items"]
        if rows == nil { rows <- map["genres"] }
    }
}

class Movie: Mappable {
    var id: Int?
    var original_title: String?
    var release_date: String?
    var poster_path: String?
    var genre_ids : [Int]?
    var overview : String?
    
    required init?(map: Map){}
    
    func mapping(map: Map) {
        id <- map["id"]
        original_title <- map["original_title"]
        release_date <- map["release_date"]
        poster_path <- map["poster_path"]
        genre_ids <- map["genre_ids"]
        overview <- map["overview"]
    }
}

class Genre: Mappable {
    var id: Int?
    var name : String?
    
    required init?(map: Map){}
    
    func mapping(map: Map) {
        id <- map["id"]
        name <- map["name"]
    }
}
