//
//  Service.swift
//  ArcChallenge
//
//  Created by Tarek on 11/5/16.
//  Copyright © 2016 Tarek. All rights reserved.
//

import Alamofire

public class Service : NSObject {
    static var manager: SessionManager {
        get {
            let m = SessionManager.default
            return m
        }
    }
}

public class APIUrls {
    static let api_key: String = "api_key=1f54bd990f1cdfb230adb312546d765d"
    static let prefix: String = "https://api.themoviedb.org/3/"
    static var listMovies:String = "\(prefix)list/1?\(api_key)"
    static var listGenre:String = "\(prefix)genre/movie/list?\(api_key)"
    static var base_url_image:String = "https://image.tmdb.org/t/p/w500"
}
