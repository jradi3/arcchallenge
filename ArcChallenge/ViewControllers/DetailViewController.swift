//
//  DetailViewController.swift
//  ArcChallenge
//
//  Created by Tarek on 11/4/16.
//  Copyright © 2016 Tarek. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {

    @IBOutlet weak var originalTitle: UILabel!
    @IBOutlet weak var poster: UIImageView!
    
    var detailItem: Movie? {
        didSet {
            // Update the view.
            self.configureView()
        }
    }

    var genres:Result<Genre>? {
        didSet {
            // Update the view.
            self.configureView()
        }
    }
    
    //
    // MARK: - ViewController life cicle

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.configureView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //
    // MARK: - Configurations

    func configureView() {
        // Update the user interface for the detail item.
        if let detail = self.detailItem {
            if let label = self.originalTitle {
                label.text =
                    "Title: \(detail.original_title!)"
                    + "\nGenre: \(printGenre(values: detail.genre_ids!))"
                    + "\nRelease Date: \(detail.release_date!)"
                    + "\nOverview: \(detail.overview!)"
                self.poster.kf.setImage(with: URL(string: "\(APIUrls.base_url_image)\(detail.poster_path!)"))
            }
        }
    }
    
    func printGenre(values : [Int]) -> String {
        if let rows = self.genres?.rows {
            var genres = String()
            for value : Genre in rows {
                if values.contains(value.id!) {
                    genres = "\(value.name!), \(genres)"
                }
            }
            return genres
        } else {
            MovieService.getGenre(success: { (result) in
                self.genres = result
            })
        }
        return ""
    }
    
}

