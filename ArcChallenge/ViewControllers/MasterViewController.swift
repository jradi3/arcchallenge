//
//  MasterViewController.swift
//  ArcChallenge
//
//  Created by Tarek on 11/4/16.
//  Copyright © 2016 Tarek. All rights reserved.
//

import UIKit

class MasterViewController: UITableViewController {

    var detailViewController: DetailViewController? = nil
    var results:Result<Movie>? = nil

    //
    // MARK: - ViewController life cicle

    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureView()
        self.loadList()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    //
    // MARK: - TableView DataSource

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return results != nil ? (results?.rows?.count)! : 0
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(forIndexPath: indexPath) as MovieTableViewCell
        if let movie = self.results?.rows?[indexPath.row] {
            cell.bind(movie: movie)
        }
        return cell
    }

    //
    // MARK: - TableView Delegate

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: "showDetail", sender: tableView.cellForRow(at: indexPath))
    }
    
    // MARK: - Segues
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let indexPath = self.tableView.indexPathForSelectedRow{
            let dest = segue.destination as! DetailViewController
            dest.detailItem = self.results?.rows?[indexPath.row]
        }
    }
    
    //
    // MARK: - Configurations
    
    func configureView(){
        self.tableView.register(MovieTableViewCell.self)
    }
    
    func loadList(){
        MovieService.getList(success: { (result) in
            self.results = result
            self.tableView.reloadData()
        })
    }
}

