# ArcChallenge

This project is build using Swift 3. Before open the project, make a pod install


The Libraries used at project:

- Alamofire
- AlamofireObjectMapper
- Kingfisher

**Alamofire** This library is used to get movies list of project.That's a easy way to get http request

**AlamofireObjectMapper** This is an extension to Alamofire which automatically converts JSON response data into swift objects using ObjectMapper.

**Kingfisher** Kingfisher is a lightweight, pure-Swift library for downloading and caching images from the web. This library is used to load all images of the project chached them.
